PUBLICO.

SEXO: Hombres y mujeres

EDAD: Entre 14 y 65 años

NACIONALIDAD: Española y latina

TIPO DE NEGOCIO: Particular

DISPOSITIVOS DE NAVEGACION: Ordenadores, telefonos y tablets

NAVEGADORES: Chrome, y firefox.

COMPORTAMIENTO: 

-Usan la aplicación para entrar en contacto con alguien y/o hablar con ellos, la charla puede ser privada, o pueden compartir datos de contacto.
Buscan personas generalmente por una relación que se mantuvo tiempo atrás, posiblemente acabó de forma brusca e inesperada. Se busca evitar el miedo al rechazo o la vergüenza en caso de que el otro les rechace, nunca se podrá ser rechazado. O tal vez saciar la curiosidad de si cierta persona se acuerda de ti.


PERMANENCIA EN LA PAGINA: 5-10min esporadicos

SECCIONES MAS VISITADAS: Perfil propio

OBJETIVOS.

PRINCIPAL:
	
	- Poner en contacto a personas entre las que hay un interes mutuo de contacto.

SECUNDARIOS:

	- Evitar el miedo al rechazo, la incomodidad y la verguenza

	- Crear un buscador de personas


ESPECIFICACIONES TECNICAS.

-

INVENTARIO DE CONTENIDO.

GRAFICOS: 

	- Logo, cabecera, iconos.

INTERNOS:

	SECCIONES PRINCIPALES: ¿A quien buscas?

	SECCIONES SECUNDARIAS: Perfil

	OTROS:  chat privado
