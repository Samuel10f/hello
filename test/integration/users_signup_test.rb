require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

	test "signup with valid information" do
		get signup_path

		#El numero indica el tamaño de la diferencia
		assert_difference 'User.count', 1 do
			post users_path, params: {user: {name: "Example User",
											 email: "user@example.com",
											 password: "password",
											 password_confirmation: "password"} }
			end	

		assert_template 'users/new'	
	end

	test "signup with invalid information" do
		get signup_path
		assert_no_difference 'User.count' do
			post users_path, user:{ name: "",
									email: "email@com",
									password: "roto2",
									password_confirmation: "roto3"}

		end
	end

end