Rails.application.routes.draw do
  get 'sessions/new'

  get 'users/new'
  get 'users/show'

  root 'users#new'

  get 'signup' 		=> 'users#new'
  get 'login' 		=> 'sessions#new'
  post 'login'		=> 'sessions#create'
  get 'actualuser' 	=> 'users#show'
  delete 'logout'	=> 'sessions#destroy'

  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
